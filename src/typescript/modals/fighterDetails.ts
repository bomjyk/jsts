import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {FighterDetail} from "../classes/FighterDetail";

export  function showFighterDetailsModal(fighter: FighterDetail) : void {
  const title: string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetail) : HTMLElement {
  const { name, attack, defense, health,source } = fighter;

  const fighterDetails : HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterInfo : HTMLElement = createElement({tagName: 'div', className: 'fighter-info'});

  const nameElement : HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement : HTMLElement = createElement({tagName: 'span', className: 'fighter-attack'});
  const defenseElement : HTMLElement = createElement({tagName: 'span', className: 'fighter-defense'});
  const healthElement : HTMLElement = createElement( {tagName: 'span', className: 'fighter-health'});
  const attributes = { src: source };
  const imageElement : HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });


  nameElement.innerText = name;
  attackElement.innerText = "Attack: "+attack;
  defenseElement.innerText = "Defense: "+defense;
  healthElement.innerText = "Health: " + health;

  fighterInfo.append(nameElement);
  fighterInfo.append(attackElement);
  fighterInfo.append(defenseElement);
  fighterInfo.append(healthElement);

  fighterDetails.append(fighterInfo);
  fighterDetails.append(imageElement);


  return fighterDetails;
}
