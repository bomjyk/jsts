import {FighterDetail} from "../classes/FighterDetail";
import {showFighterDetailsModal} from "./fighterDetails";
import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";

export  function showWinnerModal(fighter: FighterDetail) : void {

    const { name, attack, defense, health,source } = fighter;

    const fighterDetails : HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
    const fighterInfo : HTMLElement = createElement({tagName: 'div', className: 'fighter-info'});
    const nameElement : HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attributes = { src: source };
    const imageElement : HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });


    nameElement.innerText = name;

    fighterInfo.append(nameElement);

    fighterDetails.append(fighterInfo);
    fighterDetails.append(imageElement);


    showModal({ title:'Winner', bodyElement:fighterDetails});
}