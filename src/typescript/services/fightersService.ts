import { callApi } from '../helpers/apiHelper';
import { FighterDetail } from "../classes/FighterDetail";
import {Fighter} from "../classes/fighter";

export async function getFighters() : Promise<Fighter[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: Fighter[] = <Fighter[]>(await callApi(endpoint, 'GET'));
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string) : Promise<FighterDetail> {
  try {
    let endpoint: string = `details/fighter/${id}.json`;
    const apiResult: FighterDetail = <FighterDetail>(await callApi(endpoint, 'GET'));

    return apiResult;
  } catch (error) {
    throw error;
  }
  // endpoint - `details/fighter/${id}.json`;
}

