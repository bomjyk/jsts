import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import {FighterDetail} from "./classes/FighterDetail";
import {getFighterDetails} from "./services/fightersService";
import {Fighter} from "./classes/fighter";

export function createFighters(fighters: Fighter[]) : HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer : HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache : Map<string, FighterDetail> = new Map();

async function showFighterDetails(event: Event, fighter: Fighter) : Promise<void> {
  const fullInfo : FighterDetail = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<FighterDetail> {
  const fighterDetails: FighterDetail = await getFighterDetails(fighterId);
  return fighterDetails;
}

function createFightersSelector() {
  const selectedFighters: Map<string, FighterDetail> = new Map();

  return async function selectFighterForBattle(event : { target: HTMLInputElement }, fighter : Fighter) : Promise<void> {
    const fullInfo : FighterDetail = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      let winner: FighterDetail = fight(...selectedFighters.values());
      showWinnerModal(winner);
    }
  }
}
