import {FighterDetail} from "./classes/FighterDetail";

export function fight(firstFighter: FighterDetail, secondFighter: FighterDetail) : FighterDetail {
    while(firstFighter.health > 0 && secondFighter.health > 0) {
        console.log(firstFighter.name + " health: " + firstFighter.health);
        console.log(secondFighter.name + " health: " + secondFighter.health);
        console.log(firstFighter.name + " hits " + secondFighter.name);
        secondFighter.health -= getDamage(firstFighter, secondFighter);

        let tempFighter: FighterDetail = firstFighter;
        firstFighter = secondFighter;
        secondFighter = tempFighter;
    }
    console.log('Victory');
    let winner: FighterDetail;
    if(firstFighter.health > 0) {
        winner = firstFighter;
        console.log("Winner: " + firstFighter.name);
    } else {
        winner = secondFighter;
        console.log("Winner: " + secondFighter.name);
    }
    return winner;
}

export function getDamage(attacker: FighterDetail, enemy: FighterDetail) : number {
    let damage: number = getHitPower(attacker) - getBlockPower(enemy);
    if(damage < 0) {
        damage = 0;
    }
    return damage;
}

export function getHitPower(fighter: FighterDetail) : number {
    const criticalHitChange : number = Math.random() * 2 + 1;
    let hitPower: number = fighter.attack * criticalHitChange;
    return hitPower;
}

export function getBlockPower(fighter: FighterDetail) : number {
    const dodgeChange: number = Math.random() * 2 + 1;
    let blockPower: number = fighter.defense * dodgeChange;
    return blockPower;
}
